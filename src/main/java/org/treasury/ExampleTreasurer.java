package org.treasury;

import com.treasury.core.TreasuryCore;
import com.treasury.core.exceptions.AmountExceedsLimitException;
import com.treasury.core.exceptions.ClientError;
import org.bitcoinj.core.Coin;
import org.bitcoinj.core.InsufficientMoneyException;

import java.io.File;
import java.io.IOException;

public class ExampleTreasurer {

    static TreasuryCore core;

    public void testTransaction() {
        String faucetAddr = "2N8hwP1WmJrFF5QWABn38y63uYLhnJYJYTF";
        try {
            core.postFreshAddress();
            core.postBalance();
            long amount = 9000;
            Coin value = Coin.valueOf(amount);
            try {
                core.createTransaction(value, faucetAddr);
            } catch (InsufficientMoneyException e) {
                e.printStackTrace();
            } catch (AmountExceedsLimitException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClientError clientError) {
            clientError.printStackTrace();
        }
    }

    public static void main(String[] args) {
        String treasuryId = "8ed3fd53-3445-42dc-ad13-e9b66934db3a";
        File directory = new File(".");
        core = TreasuryCore.getInstance(treasuryId, directory, true);
        core.initiateKit(null);
        core.initiateTreasury(treasuryId, true);
        try {
            core.syncTreasury();
            core.postFreshAddress();
            core.setupWatch();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClientError clientError) {
            clientError.printStackTrace();
        }
    }
}