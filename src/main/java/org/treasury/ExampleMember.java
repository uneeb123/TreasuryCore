package org.treasury;

import com.treasury.core.TreasuryClient;
import com.treasury.core.TreasuryCore;
import com.treasury.core.exceptions.ClientError;
import com.treasury.core.model.Treasury;
import org.bitcoinj.core.NetworkParameters;
import org.bitcoinj.params.TestNet3Params;
import org.bitcoinj.wallet.Wallet;

import java.io.File;
import java.io.IOException;

public class ExampleMember {

    public static void main(String[] args) {
        String treasuryId = "8ed3fd53-3445-42dc-ad13-e9b66934db3a";
        File directory = new File(".");
        TreasuryCore core = TreasuryCore.getInstance(treasuryId, directory, true);
        try {
            retrieveWatchingWallet(treasuryId, directory);
            core.initiateKit(null);
            core.initiateTreasury(treasuryId, true);
            core.syncTreasury();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClientError clientError) {
            clientError.printStackTrace();
        }
    }

    /*
        TEMPORARY WORKAROUND - should live inside other components
     */
    public static void retrieveWatchingWallet(String treasuryId, File directory)
            throws IOException, ClientError {
        TreasuryClient client = new TreasuryClient(true, treasuryId);
        Treasury treasury = client.getTreasury();
        String keyPubB58 = treasury.key_data;
        long creationTime = treasury.creation_time;
        NetworkParameters params = TestNet3Params.get();
        Wallet watchingWallet = Wallet.fromWatchingKeyB58(params, keyPubB58, creationTime);
        File walletFile = new File(directory, treasuryId + ".wallet");
        watchingWallet.saveToFile(walletFile);
    }
}